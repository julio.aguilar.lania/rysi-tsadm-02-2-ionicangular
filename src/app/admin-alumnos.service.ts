/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-underscore-dangle */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Alumno } from './alumno';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminAlumnosService {

  readonly URL_ALUMNOS = environment.urlAlumnos;

  private _alumnos = new BehaviorSubject<Alumno[]>([]);
  /*
   = [{
    matricula:'ABCD123456',
    nombres:'Julio',
    apellidos:'Aguilar',
    esActivo:true,
    fechaNacimiento:new Date(1975,11,17)
  },
  {
    matricula:'XYZUVW987654',
    nombres:'Fulano',
    apellidos:'De Tal',
    esActivo:true,
    fechaNacimiento:new Date(1995,1,15)
  }
];*/
  constructor(private http: HttpClient) { }

  get alumnos() {
    return this._alumnos.asObservable(); // Entrega una copia ligera del arreglo
  }

  cargarAlumnos() {
    console.log('cargarAlumnos()');
    this.http.get<Alumno[]>(this.URL_ALUMNOS)
      .subscribe(res => {this._alumnos.next(res);}); // .then(res => {})
  }

  agregarAlumno(al: Alumno) {
    console.log('agregarAlumno');
    this.http.post(this.URL_ALUMNOS, {...al})
      .subscribe({next: res => {console.log('POST exitoso');},
        error: err => { console.log('ERROR en POST', err);}
    });
  }

  getAlumno(mat: string) {
    //return {...this._alumnos.find(a => a.matricula === mat)};
    console.log('getAlumno');
    return this.http.get<Alumno>(this.URL_ALUMNOS + '/' + mat);
  }

}
