import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormularioAlumnoPageRoutingModule } from './formulario-alumno-routing.module';

import { FormularioAlumnoPage } from './formulario-alumno.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormularioAlumnoPageRoutingModule
  ],
  declarations: [FormularioAlumnoPage]
})
export class FormularioAlumnoPageModule {}
