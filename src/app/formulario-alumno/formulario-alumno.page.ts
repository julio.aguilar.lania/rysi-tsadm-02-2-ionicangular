import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AdminAlumnosService } from '../admin-alumnos.service';
import { Alumno } from '../alumno';

@Component({
  selector: 'app-formulario-alumno',
  templateUrl: './formulario-alumno.page.html',
  styleUrls: ['./formulario-alumno.page.scss'],
})
export class FormularioAlumnoPage implements OnInit {

  nuevoAlumno: Alumno = {matricula:'',nombres:'', apellidos:'', esActivo:true, fechaNacimiento:new Date(2000,1,1)};
  constructor(private alumnosSrv: AdminAlumnosService, private navCtrl: NavController) { }

  ngOnInit() {
  }

  onCancelar() {
    this.nuevoAlumno = {matricula:'',nombres:'', apellidos:'', esActivo:true, fechaNacimiento:new Date(2000,1,1)};
    this.navCtrl.back();
  }

  onGuardarAlumno() {
    this.alumnosSrv.agregarAlumno(this.nuevoAlumno);
    this.nuevoAlumno = {matricula:'',nombres:'', apellidos:'', esActivo:true, fechaNacimiento:new Date(2000,1,1)};
    this.navCtrl.back();
  }
}
