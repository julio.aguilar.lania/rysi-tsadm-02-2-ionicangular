import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminAlumnosService } from '../admin-alumnos.service';
import { Alumno } from '../alumno';

@Component({
  selector: 'app-detalle-alumno',
  templateUrl: './detalle-alumno.page.html',
  styleUrls: ['./detalle-alumno.page.scss'],
})
export class DetalleAlumnoPage implements OnInit, OnDestroy {

  matriculaAlumno: string;
  alumno: Alumno = {matricula:'',nombres:'', apellidos:'', esActivo:true, fechaNacimiento:new Date(2000,1,1)};
  constructor(private rutaActiva: ActivatedRoute,
    private alumnosSrv: AdminAlumnosService) { }

  ngOnInit() {
    console.log('onInit()');
  }

  ngOnDestroy() {
    console.log('onDestroy()');
  }

  ionViewWillEnter() {
    console.log('willEnter');
    this.rutaActiva.paramMap.subscribe(params => {
      this.matriculaAlumno = params.get('idAlumno');
      //this.alumno = this.alumnosSrv.getAlumno(this.matriculaAlumno);
      this.alumnosSrv.getAlumno(this.matriculaAlumno)
        .subscribe(al => {this.alumno = al;});
    });
  }

  ionViewDidEnter() {
    console.log('didEnter');
  }

  ionViewWillLeave() {
    console.log('willLeave');
  }

  ionViewDidLeave() {
    console.log('didLeave');
  }
}
