export interface Alumno {
  matricula: string;
  nombres: string;
  apellidos: string;
  esActivo: boolean;
  fechaNacimiento: Date;
}
