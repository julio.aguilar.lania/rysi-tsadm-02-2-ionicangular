import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AdminAlumnosService } from '../admin-alumnos.service';
import { Alumno } from '../alumno';

@Component({
  selector: 'app-lista-alumnos',
  templateUrl: './lista-alumnos.page.html',
  styleUrls: ['./lista-alumnos.page.scss'],
})
export class ListaAlumnosPage implements OnInit {

  listaAlumnos: Alumno[];

  constructor(private navCtrl: NavController,
    private alumnosSrv: AdminAlumnosService) { }


  ngOnInit() {
    this.alumnosSrv.alumnos.subscribe(als => {this.listaAlumnos = als;});
  }

  ionViewWillEnter() {
    console.log('willEnter');
    this.alumnosSrv.cargarAlumnos();
  }

  ionViewDidEnter() {
    console.log('didEnter');
  }

  ionViewWillLeave() {
    console.log('willLeave');
  }

  ionViewDidLeave() {
    console.log('didLeave');
  }
}
